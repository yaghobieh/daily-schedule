import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Card } from 'antd';
import { changeModeView } from './reducers';
import { modes } from './modes';
import {
    UserOutlined,
    ShoppingCartOutlined,
    NodeIndexOutlined,
    CarryOutOutlined,
    AppleOutlined,
    BellOutlined,
    LogoutOutlined
} from '@ant-design/icons';
import { Menu } from 'antd';
import './style.scss';

const Controllers = props => {
    let [mode, setMode] = useState('Food');
    const [info, setInfo] = useState({
        index: null,
        icon: null,
        name: null
    });

    const changeViewOptions = viewMode => {
        setMode(viewMode);
        props.changeModeView(viewMode);
    };

    useEffect(() => {
        console.log(props.index, props.icon, props.name);
        // setInfo({
        //     index: props.index,
        //     icon: props.icon,
        //     name: props.name
        // });
    }, [])
    
    return (
        <div></div>
    )
};

const mapToProps = state => (state.daily);
export default connect(mapToProps, { changeModeView })(Controllers);