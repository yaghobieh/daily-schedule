import React from 'react';
import { connect } from 'react-redux';
import './style.scss';

const Mode = props => {
    return (
        <div>
            Mode: {props.mode} | Date picked: {props.date}
        </div>
    )
};

const mapToProps = state => (state.daily);
export default connect(mapToProps, {})(Mode);