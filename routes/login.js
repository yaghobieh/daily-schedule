const route = require('express').Router();
const jwt = require('jsonwebtoken');
const { SESSION_SECRET } = require('../global/vars');
const { USER_NOT_SAVED, USER_SAVED, USER_LOGIN_NF, WRONG_PASSWORD, LOGIN_SUCCESS } = require('../global/messages');
const isAuthorization = require('../functions/isAuthorization');
const User = require('../models/Users');

route.post('/signup', (req, res) => {
    const { email, password, fullName } = req.body;
    const user = new User();
    user.email = email;
    user.password = user.encryptPassword(password);
    user.full_name = fullName;
    user.save(err => {
        if(err) return res.status(400).json({error: err, message: USER_NOT_SAVED, code: err.code});
        return res.status(200).send({message: USER_SAVED});
    })
});

route.post('/signin', (req, res) => {
    const { email, password } = req.body;
    User.findOne({'email': email}, (err, user) => {
        if(err) return res.status(401).json({message: err});
        if(!user) return res.status(400).json({message: USER_LOGIN_NF});
        const pass = user.validPassword(password);
        if(!pass) return res.status(402).json({message: WRONG_PASSWORD});
        let token = jwt.sign(
            { email: email },
            SESSION_SECRET,
            { expiresIn: '12h'}
        );
        return res.status(200).json({token: token, message: LOGIN_SUCCESS, user});
    })
});

route.post('/loginStatus', isAuthorization, (req, res) => {
    const { email } = req.body;
    User.findOne({'email': email}, (err, user) => {
        if(err) return res.status(401).json({message: err});
        if(!user) return res.status(400).json({message: USER_LOGIN_NF});
        return res.status(200).json({error: false, message: 'LoggedIn', user: user});
    })
});

route.get('/all_user', (req, res) => {
    const userList = [];
    User.find({}, (err, users) => {
        if(err) return res.status(401).json({message: err});
        users.map(user => {
            userList.push({
                fullName: user.full_name,
                email: user.email
            });
        });
        return res.status(200).json({error: false, message: 'User list', users: userList});
    })
})

route.get('/logout', (req, res, next) => {
    console.log('LOGOUT');
});

module.exports = route;