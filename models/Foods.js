const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Food = new Schema({
    name: String
});

module.exports = mongoose.model('Food', Food);