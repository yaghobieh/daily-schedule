const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;

const User = new Schema({
    email: { type: String, unique: true, required: true},
    full_name: String,
    password: String,
    food: [{}],
    medicine: [{}],
    events: [{}],
    buy: [{}],
    tags: []
});

User.methods.encryptPassword = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(5), null);
};

User.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('User', User);