import React, { useState, useEffect } from 'react';
import { DatePicker } from 'antd';
import { connect } from 'react-redux';
import { activeDate } from '../calender/reducers';
import { changeDatePicked } from './reducers';
import './style.scss';

const DataPicker = props => {
    let [date, setDate] = useState();
    
    const onChange = (date, dateString) => {
        const chosenDate = date.format("D-MM-YYYY");
        setDate(chosenDate);
        props.changeDatePicked(chosenDate);
        props.activeDate(props.foodRule, chosenDate);
    }

    useEffect(() => {
        setDate(props.date); 
    }, [props.date])

    return (
        <DatePicker 
            onChange={onChange} 
        />
    )
};

const mapToState = state => (state.daily);
export default connect(mapToState, { changeDatePicked, activeDate })(DataPicker);