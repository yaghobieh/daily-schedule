import { SET_FOOD } from './actions';

export const setNewListOfFood = list => dispatch => {
    dispatch({
        type: SET_FOOD,
        payload: list
    });
}
