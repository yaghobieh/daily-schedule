import React, { useState, useEffect } from 'react';
import Calendar from 'react-calendar';
import { connect } from 'react-redux';
import { formatDate } from '../../helpers/functions';
import { activeDate } from './reducers';
import 'react-calendar/dist/Calendar.css';
import './style.scss';

const Calenders = props => {
    const [food, setFood] = useState([]);
    
    const onChange = date => {
        const format = formatDate(date);
        props.activeDate(props.foodRule, props.wishList, format);
    }

    const deleteFood = value => {
        console.log(value);
    }

    useEffect(() => {
        let food = [];
        if(props.active.food) {
            food = props.active.food;
            setFood(food ? food : null);
        }
    }, [props]);

    return (
        <div className="calender-container">
            <Calendar
                onChange={onChange}
                calendarType="Hebrew"
            />   
        </div>
    );
};

const mapToProps = state => (state.daily);
export default connect(mapToProps, { activeDate })(Calenders);