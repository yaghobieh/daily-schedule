const PORT = process.env.PORT || 3001;
const SESSION_SECRET = 'daily';
const DB_PATH_DEV = 'mongodb://localhost:27017/daily-schedule';
const DB_PATH = 'mongodb://daily:john2522@ds111066.mlab.com:11066/daily_scheduler';

module.exports = {
    PORT,
    SESSION_SECRET,
    DB_PATH_DEV,
    DB_PATH
}