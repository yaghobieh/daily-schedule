import React, { useState, useEffect } from 'react';
import { Alert } from 'antd';

const Notify = props => {
    const [config, setConfig] = useState();

    useEffect(() => {
      const { title, body, type, duration } = props.alert;
      setConfig({
        type: type,
        message: title,
        description: body,
        duration: duration
      });
    }, []);

    const closed = e => {
      props.close();
    }

    return (
      <div>
        {
          config ? 
          <Alert
            message={config.message}
            description={config.description}
            type={config.type}
            showIcon
            banner
            closable
            onClose={closed}
          />
          : null
        }
      </div>
    )
};

export default Notify;