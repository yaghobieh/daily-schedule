import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import StoreManager from '../helpers/StoreManager';
import rootReducer from './index';

const initializeState = new StoreManager();

const store = createStore(
    rootReducer, 
    initializeState.loadState(), 
    compose(
        applyMiddleware(thunk), 
        window.__REDUX_DEVTOOLS_EXTENSION__
        ? window.__REDUX_DEVTOOLS_EXTENSION__&& window.__REDUX_DEVTOOLS_EXTENSION__()
        : f => f
    )
);

store.subscribe(()=> {
    initializeState.saveState(store.getState());
});

export default store;