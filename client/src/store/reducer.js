import { SIGNIN, SIGNUP, CLEAR_USER } from '../components/login/action';
import { CHANGE_MODE } from '../components/controllers/actions';
import { CHANGE_DATE } from '../components/datePicker/actions';
import { SET_FOOD } from '../components/food/actions';
import { DEFAULT_MODE_VIEW } from '../helpers/vars';
import { FOOD_LIST_FILL, WISH_LIST_FILL, FILL_SHOP_TAG_LIST } from '../components/main/actions';
import { ACTIVE_DATE, REMOVE_DATE } from '../components/calender/actions';
import { TAG_LIST, SAVE_WISH_LIST } from '../components/wishList/actions';
 
const initializeState = {
    user: [],
    food: [],
    foodRule: [],
    tags: [],
    wishList: [],
    mode: DEFAULT_MODE_VIEW,
    wishListBuild: [],
    shopTagList: [],
    date: null,
    active: {}
};

export default function(state = initializeState, action) {
    switch(action.type) {
        case SIGNIN:
            return {
                ...state,
                user: action.payload
            };
        case SIGNUP: 
            return {
                ...state,
                user: action.payload
            };
        case CHANGE_MODE: 
            return {
                ...state,
                mode: action.payload
            }
        case CHANGE_DATE: 
            return {
                ...state,
                date: action.payload
            }
        case SET_FOOD:
            return {
                ...state,
                food: action.payload
            }
        case CLEAR_USER:
            return {
                ...state,
                user: [],
                food: [],
                foodRule: [],
                mode: DEFAULT_MODE_VIEW,
                date: null,
                active: {},
                wishList: {},
                wishListBuild: [],
                tags: [],
                shopTagList: []
                
            };
        case FOOD_LIST_FILL:
            return {
                ...state,
                foodRule: action.payload
            }
        case ACTIVE_DATE:
            return {
                ...state,
                wishListBuild: action.payload.date !== state.date ? [] : state.wishListBuild, 
                tags: [],
                date: action.payload.date,
                active: {
                    date: action.payload.date,
                    food: action.payload.list[0] ? action.payload.list[0].food : [],
                    wishList: action.payload.wish ? action.payload.wish[0] : []
                }
            }
        case TAG_LIST:
            return {
                ...state,
                tags: action.payload
            }
        case REMOVE_DATE: 
            return {
                ...state,
                active: {}
            }
        case WISH_LIST_FILL:
            return {
                ...state,
                wishList: action.payload
            }
        case SAVE_WISH_LIST: 
            return {
                ...state, 
                wishListBuild: action.payload 
            }
        case FILL_SHOP_TAG_LIST:
            return {
                ...state,
                shopTagList: action.payload
            }
        default:
            return state;
    }
}