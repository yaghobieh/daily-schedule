import { CHANGE_MODE } from './actions';

export const changeModeView = mode => dispatch => {
    dispatch({
        type: CHANGE_MODE,
        payload: mode
    });
}