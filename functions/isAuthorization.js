const jwt = require('jsonwebtoken');
const { SESSION_SECRET } = require('../global/vars');

const isAuthorization = (req, res, next) => {
    const token = req.cookies.user;
    if (!token) {
        return res.status(401).json({error: true, code: 2, message: 'Unauthorized: No token provided'});
    } else {
        jwt.verify(token, SESSION_SECRET, function (err, decoded) {
            if (err) {
                return res.status(401).json({error: true, code: 2, message: 'Unauthorized: Invalid token'});
            } else {
                // req.email = decoded.email;
                next();
            }
        });
    }
}

module.exports = isAuthorization;