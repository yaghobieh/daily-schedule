import axios from 'axios';

export const transport = axios.create({
    origin: "http://localhost:3003",
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
    },
    withCredentials: true,
});

export const formatDate = date => {
    const d = new Date(date);
    const monthLength = JSON.stringify(d.getMonth()).length;
    const month = monthLength !== 1 ? '' + (d.getMonth() + 1) : '0' + (d.getMonth() + 1);
    const day = '' + d.getDate();
    const year = d.getFullYear();
    const formatDate = day + '-' + month + '-' + year;
    return day + '-' + month + '-' + year;
}