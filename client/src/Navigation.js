import React, { useState, useEffect } from 'react';
import Signin from './components/login/Signin';
import Main from './components/main/Main';
import SignUp from './components/login/Signup';
import { Menu, Badge } from 'antd';
import {
  UserOutlined,
  ShoppingCartOutlined,
  NodeIndexOutlined,
  CarryOutOutlined,
  AppleOutlined,
  BellOutlined,
  LogoutOutlined
} from '@ant-design/icons';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useHistory
} from "react-router-dom";
import { changeModeView } from './components/controllers/reducers';
import { connect } from 'react-redux';
import { clearPropsFromUser } from './components/login/reducers';
import Cookies from 'js-cookie';
import './components/main/style.scss';

const Navigation = props => {
    const [ counters, setCounter ] = useState({
        food: null,
        wishList: null,
        medicine: null,
        events: null,
        tagShopList: null
    });

    useEffect(() => {
        const { wishList, foodRule, shopTagList } = props;
        if(shopTagList) {
            setCounter({
                ...counters,
                food: foodRule.length,
                wishList: wishList.length,
                tagShopList: shopTagList.length
            });
        }
    }, [props]);

    const signOut = () => {
        Cookies.remove('user');
        props.clearPropsFromUser();
        window.location.href = '/signin';
    }

    const changeViewOptions = viewMode => {
        props.changeModeView(viewMode);
    };

    const signUpMove = () => {
        if(!props.user.email) {
            window.location.href = '/signin';
        } 
    }

    return (
        <div>
            <Router>
                <Switch>
                    <Route path="/signup">
                        <SignUp />
                    </Route>
                    <Route path="/signin">
                        <Signin />
                    </Route>
                    <Route path="/">
                        <Main />
                    </Route>
                </Switch>
            </Router>
            <div className="nav-container">
                <Menu
                defaultSelectedKeys={['1']}
                mode="horizontal"
                theme="dark"
                >
                    <Menu.Item key="0" onClick={ () => signUpMove() }>
                        <UserOutlined />
                        {
                            props.user.fullName ?
                            props.user.fullName : 'אינך מחובר/ת'
                        }
                    </Menu.Item>
                    <Menu.Item key="1" onClick={() => changeViewOptions('Food')}>
                        <AppleOutlined /> Food 
                        {
                            counters.food ? 
                            <Badge count={counters.food} style={{marginLeft: '10px'}}></Badge> : 
                            '' 
                        }
                    </Menu.Item>
                    <Menu.Item key="2" onClick={() => changeViewOptions('Event')}>
                        <CarryOutOutlined /> New event
                    </Menu.Item>
                    <Menu.Item key="3"
                     onClick={() => changeViewOptions('Medicine')}>
                        <NodeIndexOutlined /> Medicine 
                    </Menu.Item>
                    <Menu.Item key="4" onClick={() => changeViewOptions('Buy')}>
                        <ShoppingCartOutlined /> Wish list
                        { 
                            counters.wishList ? 
                            <Badge count={counters.wishList} style={{marginLeft: '10px'}}></Badge> : 
                            '' 
                        }
                    </Menu.Item>
                    <Menu.Item key="5" onClick={ () => changeViewOptions('tagShopList') }>
                        <BellOutlined /> Tag lists 
                        { 
                            counters.tagShopList ? 
                            <Badge count={counters.tagShopList} style={{marginLeft: '10px'}}></Badge> : 
                            '' 
                        }
                    </Menu.Item>
                    <Menu.Item key="6"  onClick={signOut}>
                        <LogoutOutlined />
                        SignOut
                    </Menu.Item>
                </Menu>
            </div>
        </div>
    )
};

const mapToProps = state => (state.daily);
export default connect(mapToProps, { clearPropsFromUser, changeModeView })(Navigation);