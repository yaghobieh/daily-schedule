import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Button } from 'antd';
import Calenders from '../calender/Calender';
import Food from '../food/Food';
import DatePicker from '../datePicker/DatePicker';
import WishList from '../wishList/WishList';
import TagShopList from '../tagShopList/TagShopList';
import axios from 'axios';
import Mode from '../mode/Mode';
import { fillFoodList, submitNewFood, fillWishList, fillShopListTagged } from './reducers';
import Notify from '../notify/Notify';
import { useHistory } from 'react-router-dom';
import './style.scss';

const Main = props => {
    const history = useHistory();
    let [mode, setMode] = useState();
    let [date, setDate] = useState();
    let [foodList, setFoodList] = useState();
    const [dayList, setDayList] = useState();
    const [alert, setAlert] = useState({
        title: '',
        body: '',
        type: '',
        duration: 0,
        show: false
    });
    let [status, setStatus] = useState({
        load: false,
        disable: true
    });

    useEffect(() => {
        setMode(props.mode);
        setDate(props.date);
        setFoodList(props.food);
        if(props.food && props.food.length > 0 && props.date) {
            setStatus({
                ...status,
                disable: false
            });
        }
    }, [props]);
    
    useEffect(() => {
        setMode(props.mode);
        setDate(props.date);
        setFoodList(props.food);
        if(props.food.length > 0 && props.date) {
            setStatus({
                ...status,
                disable: false
            });
        }
    }, []);

    useEffect(() => {
        axios.post('user/loginStatus', {email: props.user.email})
            .then(res => {
                const { food, buy, tags } = res.data.user;
                props.fillFoodList(food);
                props.fillWishList(buy);
                props.fillShopListTagged(tags);
            })
            .catch(err => {
                history.push('/signin');
            });
    }, []);

    useEffect(() => {
        axios.post('user/loginStatus', {email: props.user.email})
            .then(res => {
                const { food, buy, tags } = res.data.user;
                props.fillFoodList(food);
                props.fillWishList(buy);
                props.fillShopListTagged(tags);
            })
            .catch(err => {
                history.push('/signin');
            });
    }, [alert])

    const submit = e => {
        e.preventDefault();
        const updateList = {
            date: date,
            food: foodList,
            email: props.user.email
        };
        axios.post('food/set-user-food-list', updateList)
        .then(res => {
            if(res.status !== 200) {
                const list = res.data.list.food;
                setDayList(list);
                axios.post('food/update-food-user', updateList)
                    .then(resTwo => {
                        if(resTwo.status === 200) {
                            setAlert({
                                title: 'Status message',
                                body: 'עודכן בהצלחה',
                                type: 'success',
                                duration: 4,
                                show: true
                            });
                        } else {
                            setAlert({
                                title: 'Notify!',
                                body: 'משהו השתבש',
                                type: 'warning',
                                duration: 4,
                                show: true
                            });
                        }
                    })
                    .catch(err => console.log(err))
            } else {
                setAlert({
                    title: 'Notify!',
                    body: 'נוצר בהצלחה',
                    type: 'success',
                    duration: 4,
                    show: true
                });
            }
        })
        .catch(err => console.log(err));
        const wishList = {
            list: props.wishListBuild,
            email: props.user.email,
            date: props.date,
            tags: props.tags
        };
        axios.post('wish_list/add_list', wishList)
            .then(res => {
                if(res.status === 200) {
                    setAlert({
                        title: 'Status message',
                        body: 'נוצר בהצלחה',
                        type: 'success',
                        duration: 4,
                        show: true
                    });
                } else {
                    axios.post('wish_list/update_list', wishList)
                        .then(resNext => {
                            if(resNext.status === 200) {
                                setAlert({
                                    title: 'Status message',
                                    body: 'נוצר בהצלחה',
                                    type: 'success',
                                    duration: 4,
                                    show: true
                                });
                            } else {
                                setAlert({
                                    title: 'Notify!',
                                    body: 'נוצר בהצלחה',
                                    type: 'success',
                                    duration: 4,
                                    show: true
                                });
                            }
                        })
                        .catch(err => console.log(err));
                }
            })
            .catch(err => console.log(err));

        axios.post('user/loginStatus', {email: props.user.email})
        .then(res => {
            const foodList = res.data.user.food;
            const wishList = res.data.user.buy;
            props.fillFoodList(foodList);
            props.fillWishList(wishList);
        })
        .catch(err => {
            history.push('/signin');
        });
    }

    const closed = () => {
        setAlert({
            title: '',
            body: '',
            type: '',
            duration: 0,
            show: false
        });
    }

    return (
        <div className="main-container">
            <div className="inner">      
                <Mode />
                {/*<Profile />*/}
                {
                    alert.show ?
                    <Notify 
                        alert={alert}
                        close={closed}
                    /> : null
                }
                {/* <DatePicker /> */}

                {
                    mode === 'Food' ?
                    <Food /> :
                    mode === 'Event' ?
                    null :
                    mode === 'Buy'?
                    <WishList /> : 
                    mode === 'Medicine' ?
                    null :
                    mode === 'tagShopList' ?
                    <TagShopList /> : null
                }
                {
                    mode !== 'tagShopList' ?
                    <Button 
                    type="primary" 
                    danger 
                    className="submit-btn" 
                    loading={status.load}
                    // disabled={props.date}
                    onClick={submit}
                    >
                        Save
                    </Button> : null
                }
                {
                    mode !== 'tagShopList' ?
                    <Calenders /> : null
                    
                }
            </div>
        </div>
    )
};

const mapToProps = state => (state.daily);
export default connect(mapToProps, { 
    fillFoodList, 
    submitNewFood,
    fillWishList,
    fillShopListTagged 
})(Main);