const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('morgan');
const session = require('express-session');
const cookie = require('cookie-parser');
const MongoStore = require('connect-mongo')(session); 
const path = require('path');
const { PORT, DB_PATH_DEV, SESSION_SECRET, DB_PATH } = require('./global/vars');
const app = express();

mongoose.connect(DB_PATH, 
  { 
    useUnifiedTopology: true, 
    useNewUrlParser: true, 
    useCreateIndex: true 
  }, 
  err => err ? console.log(`DB server not working, ${err} path => ${DB_PATH}`) : console.log('DB is running')
);
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

const corsConfig = {
  origin: true,
  credentials: true,
};
app.use(cors(corsConfig));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(logger('dev'));
app.use(cookie());
app.use(session({ 
  secret: SESSION_SECRET, 
  resave: false, 
  saveUninitialized: true,
  store: new MongoStore({ mongooseConnection: mongoose.connection }),
  cookie: { maxAge: 180 * 60 * 1000 , secure: false }
}));

app.use('/food', require('./routes/food'));
app.use('/wish_list', require('./routes/wishList'));
app.use('/user', require('./routes/login'));

app.use(express.static('./client/build'));
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../client', 'build', 'index.html'));    
});

app.listen(process.env.PORT || PORT, err => err ? console.log(`Server not working, ${err}`) : console.log('Server is running PORT:', PORT));
