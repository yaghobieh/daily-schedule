export const SUBMIT = 'submit';
export const FOOD_LIST_FILL = 'food_list_fill';
export const WISH_LIST_FILL = 'wish_list_fill';
export const FILL_SHOP_TAG_LIST = 'fill_shop_tag_list';