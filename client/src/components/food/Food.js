import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Select } from 'antd';
import { setNewListOfFood } from './reducers';
import './style.scss';

const foodList = ['חביתה', 'בורקס', 'אורז', 'דג סלומון', 'ירקות מוקפצים ובשר', 'בשר בתנור'];

const Food = props => {
    const [ state, setState ] = useState();
    const { Option } = Select;
    const children = [];

    // for (let i = 10; i < 36; i++) {
    //     children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
    // }
    foodList.map(food => {
        children.push(
            <option key={food}>
            {food}
            </option>
        )
    });

    const handleChange = value => {
        setState(value);
        props.setNewListOfFood(value);
    }

    useEffect(() => {
      setState(props.active.food);
    }, [props.active.food])

    return (
        <div className="food">
            <Select
                mode="multiple"
                style={{ width: '100%' }}
                placeholder="Food list"
                value={state}
                onChange={handleChange}
            >
                {children}
            </Select>
        </div>
    )
};

const mapToProps = state => (state.daily);
export default connect(mapToProps, { setNewListOfFood })(Food);