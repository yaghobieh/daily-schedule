export const FORMS_ERRORS = {
    INPUT_REQUIRE: 'שדה זה הוא חובה',
    INPUT_PASSWORD: 'מה הסיסמא שלך? זהו שדה חובה'
};
export const LOGIN_ERROR = {
    TITLE: 'שגיאה בהתחברות',
    BODY: 'אנא בדוק את הערכים שהכנסת, אחד או יותר מהם לא נכון',
    TYPE: 'warning',
};