import React from 'react';
import Navigation from './Navigation';
import 'antd/dist/antd.css';

function App() {
  return (
    <div className="App">
      <Navigation />
    </div>
  );
}

export default App;
