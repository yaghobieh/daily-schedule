import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import Nofity from '../notify/Notify';
import { FORMS_ERRORS } from '../../helpers/messages';
import './style.scss';

const SignUp = props => {
    const history = useHistory();
    let [showAlert, setShowAlert] = useState({
        title: '',
        body: '',
        type: '',
        duration: 0,
        show: false
    });
    let [error, setError] = useState({
        hasError: true,
        message: null
    });

    const onFinish = e => {
        e.preventDefault();
        const register = {
            fullName: e.target[0].value,
            email: e.target[1].value,
            password: e.target[2].value,
            sPassword: e.target[3].value
        };
        if(register.username === '' || register.email === '' || register.password === '' || register.sPassword === '') {
            setError({
                hasError: true,
                message: 'אנא מלא/י את כלל השדות בטופס'
            });
        } else if (register.password !== register.sPassword) {
            setError({
                hasError: true,
                message: 'הסיסמאות אינן תואמות'
            });
        } else if(register.password === register.sPassword) {
            if(register.password.length <= 5) {
                setError({
                    hasError: true,
                    message: 'הסיסמא חייבת להיות מעל ל-5 תווים'
                });
            }
        } else {
            setError({
                hasError: false,
                message: null
            });
        }

        axios.post('user/signup', register)
            .then(res => {
                setShowAlert({
                    title: 'נרשם בהצלחה',
                    body: 'המשתמש שלך נרשם בהצלחה',
                    type: 'success',
                    duration: 0,
                    show: true
                });
                history.push('/signin');
            })
            .catch(err => {
                if(err.response.data.code === 11000) {
                    setShowAlert({
                        title: 'בעייה בעת ההרשמה',
                        body: 'משתמש זה כבר קיים במערכת',
                        type: 'error',
                        duration: 0,
                        show: true
                    });
                }
            });
    }

    const close = () => {
        setShowAlert({
            title: '',
            body: '',
            type: '',
            duration: 0,
            show: false
        });
    }

    return (
        <div className="sigin">
            <div className="form-block"> 
            {
                showAlert.show ?
                <Nofity 
                    alert={showAlert}
                    close={close}
                /> : null
            }           
                <form 
                    onSubmit={onFinish}
                >
                    <div className="group">
                        <p>שם משתמש</p>
                        <input 
                            type="string"
                            name="userName"
                            placeholder="My user-name"
                        />
                    </div>
                    <div className="group">
                        <p>כתובת מייל</p>
                        <input 
                            type="email"
                            name="email"
                            placeholder="you@you.co.il"
                        />
                    </div>
                    <div className="group">
                        <p>סיסמא</p>
                        <input 
                            type="password"
                            name="password"
                            placeholder="••••••"
                        />
                    </div>
                    <div className="group">
                        <p>חזור על אותה הסיסמא</p>
                        <input 
                            type="password"
                            name="sPassword"
                            placeholder="••••••"
                        />
                        {
                            error.hasError ?
                            <div className="error">{error.message}</div> : null
                        }
                    </div>
                    <button>הרשם</button>
                </form>
            </div>
        </div>
    ) 
}

const mapToProps = (state) => (state.daily);
export default connect(mapToProps, {})(SignUp);