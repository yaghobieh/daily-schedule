const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const { SESSION_SECRET } = require('./global/vars');
const { USER_LOGIN_NF, WRONG_PASSWORD } = require('./global/messages.js');
const passportJWT = require('passport-jwt');
const JWTStrategy   = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
const User = require('./models/Users');

passport.serializeUser(function(user, done){
    done(null, user.id)
});

passport.deserializeUser(function(id, done){
    User.findById(id, (err, user) => {
        done(err, user)
    })
});

passport.use('local.signup', new LocalStrategy({
    usernameField: 'email', passwordField: 'password', passReqToCallback: true
}, function(req, email, password, done){
    const user = new User();
    user.email = email;
    user.password = user.encryptPassword(password);
    user.full_name = req.body.fullName;

    user.save((err, user) => {
        if(err) return done(err);
        return done(null, user);
    });
}));

passport.use('local.signin', new LocalStrategy({
    usernameField: 'email', passwordField: 'password', passReqToCallback: true
}, function(req, email, password, done){
    User.findOne({'email': email}, (err, user) => {
        if(err) return done(err);
        if(!user) return done(null, false, {message: USER_LOGIN_NF});
        if(!user.validPassword(password)) return done(null, false, {message: WRONG_PASSWORD});
        return done(null, user);
    })
}));
