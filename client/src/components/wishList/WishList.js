import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import Spinner from '../spinner/Spinner';
import { addTag, saveIntoWishList } from './reducers';
import { Input, Select, Form, Button } from 'antd';
import { ThunderboltOutlined, EditOutlined, ScissorOutlined, CheckCircleTwoTone, CloseCircleOutlined } from '@ant-design/icons';
import { REMOVE, EDIT, CHANGE_STATUS } from '../../helpers/vars';
import './style.scss';

const WishList = props => {
    const [users, setUsers] = useState();
    const [changes, setChanges] = useState({
        item: null,
        amount: 0,
        status: true
    });
    const [items, setItems] = useState();
    const [tags, setTags] = useState();
    const { Option } = Select;
    const children = [];

    useEffect(() => {
        axios.get('user/all_user')
            .then(res => {
                const users = res.data.users;
                setUsers(users);
            })
            .catch(err => console.log(err));
    }, []);

    useEffect(() => {
        if(props.wishListBuild.length > 0) {
            const list = props.wishListBuild;
            setItems(list);
        } else if (props.active.wishList) {
            const { tags, date, list } = props.active.wishList;
            const tagsNew = props.tags;
            if(tagsNew.length !== 0) {
                setTags(tagsNew);
            } else {
                setTags(tags);
            }
            setItems(list);
        } else {
            setTags();
            setItems();
        }
    }, [props]);

    const handleChange = value => {
        props.addTag(value);
    }

    const submit = () => {
        let arr = items ? [...items] : [];
        let isExist = false;
        if(arr.length > 0) {            
            arr = items.map((item, index) => {
                if(item.item === changes.item) {
                    isExist = true;
                    return items[index] = changes;
                }
                return item;
            });
        }
        if(!isExist) {
            arr.push(changes);
        }
        // setItems(arr);
        setChanges({
            item: null,
            amount: 0,
            status: true
        });
        props.saveIntoWishList(arr);
    }

    const handleItemChange = e => {
        setChanges({
            ...changes,
            item: e.target.value
        });
    }

    const handleAmountChange = e => {
        setChanges({
            ...changes,
            amount: e.target.value
        })
    }

    const doAction = (thisItem, todo) => {
        if(todo === REMOVE) {
            const arr = items.filter(i => i.item !== thisItem.item)
            setItems(arr);
            props.saveIntoWishList(arr);
        }
        if(todo === CHANGE_STATUS) {
            let listArr = [...items];
            listArr = items.map((i, index) => {
                if(i.item === thisItem.item) {
                    listArr[index].status = !items[index].status
                    return listArr[index]; 
                }
                return i;
            });
            setItems(listArr);
        }
        if(todo === EDIT) {
            setChanges({
                item: thisItem.item,
                amount: thisItem.amount,
                status: thisItem.status
            });
        }
    }
    
    if(users) {
        return (
            <div className="wishL">
                <Select
                    mode="multiple"
                    style={{ width: '100%' }}
                    placeholder="Select users to tag"
                    onChange={handleChange}
                    value={tags}
                >
                    {
                        users.map(user => {
                            if(user.email !== props.user.email) {
                                return <Option key={user.fullName} 
                                            value={user.email}
                                        >
                                            {user.fullName ? user.fullName : user.fullName}
                                        </Option>
                            }
                        })
                    }
                </Select>
                <Form
                    onFinish={submit}
                >
                    <Form.Item
                        rules={[{ required: true }]}
                        style={{ marginTop: 10, width: '100%' }}
                    >
                        <Input  
                            onChange={handleItemChange}
                            prefix={<ThunderboltOutlined className="site-form-item-icon" />}
                            value={changes.item}
                            type="text"
                        />
                        <Input  
                            onChange={handleAmountChange}
                            style={{ marginTop: 10 }}
                            value={changes.amount}
                            type="number"
                        />
                    </Form.Item>
                    <Button 
                        style={{ width: '100%' }}
                        danger 
                        htmlType="submit"
                        disabled={!changes.item}
                    >Add item</Button>
                </Form>
                {
                    items? 
                    <div className="items-container">
                       {
                           items.map(item => {
                               return <div className="item">
                                        <div className="controllers">
                                            <EditOutlined onClick={() => doAction(item, EDIT)} />
                                            <ScissorOutlined onClick={() => doAction(item, REMOVE)}/>
                                            {
                                                item.status ?
                                                <CheckCircleTwoTone onClick={() => doAction(item, CHANGE_STATUS)} /> : 
                                                <CloseCircleOutlined style={{color: 'red'}} 
                                                    onClick={() => doAction(item, CHANGE_STATUS)}
                                                />
                                            }
                                        </div>
                                        <div className="text" style={{ 'textDecoration' : !item.status ? 'line-through' : null }}>
                                            {item.item} <u>כמות:</u> {item.amount}
                                        </div>
                                    </div>
                           })
                       }
                    </div> : null
                }
            </div>
        )
    } else {
        return <Spinner />
    }
}

const stateToProps = (state) => (state.daily);
export default connect(stateToProps, { addTag, saveIntoWishList })(WishList);