import { SIGNIN, SIGNUP, CLEAR_USER } from './action';

export const signIn = user => dispatch => {
    dispatch({
        type: SIGNIN,
        payload: user
    });
};

export const signUp = user => dispatch => {
    dispatch({
        type: SIGNUP,
        payload: user
    });
};

export const clearPropsFromUser = () => dispatch => {
    localStorage.removeItem('redux');
    dispatch({
        type: CLEAR_USER,
        payload: null
    });
};