export const SIGNIN = 'signin';
export const SIGNUP = 'signup';
export const SIGNOUT = 'signout';
export const CLEAR_USER = 'clear_user';