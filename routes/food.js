const route = require('express').Router();
const User = require('../models/Users');
const Foods = require('../models/Foods');
const { FOOD_LIST_EMPTY, FOOD_SAVED, FOOD_ALREADY_EXIST } = require('../global/messages');

route.get('/food-list', (req, res) => {
    Foods.find({}, (err, foods) => {
        if(err || !foods)
            return res.status(400).json({message: FOOD_LIST_EMPTY, err: err});
        return res.status(200).json({foodList: foods});
    });
});

route.post('/set-food', async (req, res, next) => {
    const { name } = req.body;
    const food = new Foods({
        name: name
    });
    await food.save(err => {
        if(!err) return res.status(200).json({message: `${name} ${FOOD_SAVED}`});
    });
});

route.post('/set-user-food-list', (req, res) => {
    const { food, date, email } = req.body;
    User.findOne({'email': email}, (err, user) => {
        let foodList = [];
        let found = false;
        user.food.map(f => {
            if(f.date === date) {
                foodList = f;
                found = true;
            } 
        })
        const arr = {
            food: food,
            date: date
        };
        if(!found) {      
            user.food.push(arr);
            user.save(err => {
                if(!err) return res.status(200).json({message: 'OK'});
            });
        } else {
            return res.status(201).json({message: FOOD_ALREADY_EXIST, list: foodList});
        }
    })
});

route.post('/update-food-user', (req, res) => {
    const { food, date, email } = req.body;
    User.findOne({'email': email}, (err, user) => {
        const updatedList = [...user.food];
        user.food.map((f, index) => {
            if(f.date === date) 
                return updatedList[index] = {
                    food: food,
                    date: date
                };
        });
        user.food = updatedList;
        User.findByIdAndUpdate(user._id, user, {upsert: true}, (err, update) => {
            if(!err) return res.status(200).json({message: 'OK', err: null});
        });
    });
});

module.exports = route;