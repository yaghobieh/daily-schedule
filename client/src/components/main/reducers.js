import { FOOD_LIST_FILL, SUBMIT, WISH_LIST_FILL, FILL_SHOP_TAG_LIST } from './actions';

export const fillShopListTagged = list => dispatch => {
    dispatch({
        type: FILL_SHOP_TAG_LIST,
        payload: list
    });
};

export const fillFoodList = list => dispatch => {
    dispatch({
        type: FOOD_LIST_FILL,
        payload: list
    });
};

export const fillWishList = list => dispatch => {
    dispatch({
        type: WISH_LIST_FILL,
        payload: list
    });
};

export const submitNewFood = foodList => dispatch => {
    dispatch({
        type: SUBMIT,
        payload: foodList
    });
};
