const route = require('express').Router();
const User = require('../models/Users');
const isAuthorization = require('../functions/isAuthorization');
const { WISHLIST_ALREADY_EXIST } = require('../global/messages');
const { sendWishList } = require('../functions/email');

route.post('/add_list', isAuthorization, (req, res) => {
    const { list, date, email, tags } = req.body;
    User.findOne({'email': email}, (err, user) => {
        const userWishList = user.buy;
        let wishList = [];
        let found = false;
        userWishList.map(userList => {
            if(userList.date === date) {
                wishList = userList;
                found = true;
            }
        });
        const arr = {
            list: list,
            date: date,
            tags: tags
        };
        if(!found) {
            user.buy.push(arr);
             // Loop trow tag emails and create user 
            tags.map(emailToSend => {
                User.findOne({email: emailToSend}, (err, userM) => {
                    const userTag = {
                        list: list,
                        date: date,
                        tagger: email
                    };
                    userM.tags.push(userTag);
                    userM.save();
                    sendWishList(emailToSend, arr);
                });
            })
            user.save(err => {
                if(!err) return res.status(200).json({message: 'OK'});
            });
        } else {
            return res.status(201).json({message: WISHLIST_ALREADY_EXIST, list: wishList});
        }
    });
});

route.post('/update_list', isAuthorization, (req, res) => {
    const { list, date, email, tags } = req.body;
    User.findOne({'email': email}, (err, user) => {
        const updateWishList = [...user.buy];
        user.buy.map((b, index) => {
            if(b.date === date) {
                return updateWishList[index] = {
                    list: list,
                    date: date,
                    tags: tags
                };
            }
        });
        user.buy = updateWishList;
        // Find user hw tag in and update
        tags.map(tag => {
            User.findOne({'email': tag}, (err, userM) => {
                const userWishListTag = {
                    list: list,
                    date: date,
                    name: email
                };
                const userBuyList = [...userM.tags];
                userM.tags.map((b, index) => {
                    if(b.tagger === email, b.date === date) {
                        userBuyList[index] = userWishListTag;
                    }
                });
                sendWishList(tag, userWishListTag);
                userM.tags = userBuyList;
                userM.save();
            });
        })
        User.findByIdAndUpdate(user._id, user, {upsert: true}, (err, update) => {
            if(!err) return res.status(200).json({message: 'OK', err: null});
        });
    });
});

module.exports = route;