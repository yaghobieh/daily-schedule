const FOOD_LIST_EMPTY = 'רשימת המאכלים ריקה';
const FOOD_SAVED = 'נוסף בהצלחה';
const USER_LOGIN_NF = 'משתמש לא נמצא';
const USER_NOT_SAVED = 'משתמש לא נשמר, אחד הערכים או יותר לא נכונונים';
const WRONG_PASSWORD = 'סיסמא לא נכונה';
const USER_SAVED = 'משתמש נרשם בהצלחה';
const LOGIN_SUCCESS = 'משתמש התחבר בהצלחה';
const FOOD_ALREADY_EXIST = 'תאריך זה מלא, אפשר רק לעדכן';
const WISHLIST_ALREADY_EXIST = 'יש רשימה בתאריך זה';

module.exports = {
    FOOD_LIST_EMPTY,
    FOOD_SAVED,
    USER_NOT_SAVED,
    USER_SAVED,
    USER_LOGIN_NF,
    LOGIN_SUCCESS,
    WRONG_PASSWORD,
    FOOD_ALREADY_EXIST,
    WISHLIST_ALREADY_EXIST
};