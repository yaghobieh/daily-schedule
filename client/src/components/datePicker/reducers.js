import { CHANGE_DATE } from './actions.js';

export const changeDatePicked = date => dispatch => {
    dispatch({
        type: CHANGE_DATE,
        payload: date
    });
};