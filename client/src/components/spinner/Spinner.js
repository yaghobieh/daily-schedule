import React from 'react';
import { Spin } from 'antd';

const Spinner = () => {
    return (
        <div style={{textAlign: 'center'}}>
            <Spin size="large" style={{padding: '20px'}} />
        </div>
    )
};

export default Spinner;