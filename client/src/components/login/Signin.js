import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { signIn, clearPropsFromUser } from './reducers';
import axios from 'axios';
import Cookies from 'js-cookie';
import { useHistory } from 'react-router-dom';
import { transport } from '../../helpers/functions';
import Nofity from '../notify/Notify';
import { FORMS_ERRORS, LOGIN_ERROR } from '../../helpers/messages';
import { Button } from 'antd';
import './style.scss';

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};

const Signin = props => {
    const history = useHistory();
    let [showAlert, setShowAlert] = useState({
        title: '',
        body: '',
        type: '',
        duration: 0,
        show: false
    });
    const [errors, setErrors] = useState({
        email: null,
        password: null
    });
    
    const onFinish = e => {
        e.preventDefault();
        if(e.target[0].value === '') {
            const email_err = FORMS_ERRORS.INPUT_REQUIRE;
            setErrors({
                password: null,
                email: email_err
            });
            return;
        }
        if(e.target[1].value === '') {
            const pass_err = FORMS_ERRORS.INPUT_PASSWORD;
            setErrors({
                email: null,
                password: pass_err
            });
            return;
        }
        const user = {
            email: e.target[0].value,
            password: e.target[1].value
        };
        transport.post('user/signin', user) 
            .then(res => {
                if(res.status === 200) {
                    setShowAlert({
                        title: '',
                        body: '',
                        type: '',
                        duration: 0,
                        show: false
                    });
                    Cookies.set('user', res.data.token, { expires: 7 });
                    const user = {
                        fullName: res.data.user.full_name,
                        email: res.data.user.email,
                        token: res.data.token
                    };
                    props.signIn(user);
                    history.push('/');
                }
            })
            .catch(err => {
                const { TITLE, BODY, TYPE } = LOGIN_ERROR;
                setShowAlert({
                    title: TITLE,
                    body: BODY,
                    type: TYPE,
                    duration: 0,
                    show: true
                });
                props.clearPropsFromUser();
            });
    };

    useEffect(() => {
        axios.post('user/loginStatus', {email: props.user.email})
            .then(res => {
                history.push('/');
            })
            .catch(err => {
                Cookies.remove('user');
                props.clearPropsFromUser();
            });
    }, []);

    const close = () => {
        setShowAlert({
            title: '',
            body: '',
            type: '',
            duration: 0,
            show: false
        });
    }

    const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
    };

    const register = e => {
        history.push('/signup');
    }

    return (
        <div className="sigin">
            <div className="form-block"> 
            {
                showAlert.show ?
                <Nofity 
                    alert={showAlert}
                    close={close}
                /> : null
            }           
                <form 
                    onSubmit={onFinish}
                >
                    <div className="group">
                        <p>כתובת מייל</p>
                        <input 
                            type="email"
                            name="email"
                            placeholder="you@you.co.il"
                        />
                        {
                            errors.email ?
                            <div className="error">{errors.email}</div> : null
                        }
                    </div>
                    <div className="group">
                        <p>סיסמא</p>
                        <input 
                            type="password"
                            name="password"
                            placeholder="••••••"
                        />
                        {
                            errors.password ?
                            <div className="error">{errors.password}</div> : null
                        }
                    </div>
                    <button>התחבר</button>
                </form>
                <Button onClick={register} type="link">אין לך חשבון? הרשם</Button>
            </div>
        </div>
    )
};

const stateToMap = state => (state.daily);
export default connect(
    stateToMap, {
        signIn, 
        clearPropsFromUser
    }
)(Signin);