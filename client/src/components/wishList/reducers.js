import { TAG_LIST, SAVE_WISH_LIST } from './actions';

export const addTag = tags => dispatch => {
    dispatch({
        type: TAG_LIST,
        payload: tags
    });
};

export const saveIntoWishList = list => dispatch => {
    dispatch({
        type: SAVE_WISH_LIST,
        payload: list
    });
};