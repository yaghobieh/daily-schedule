import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Modal, Button, Space } from 'antd';
import Spinner from '../spinner/Spinner';
import './style.scss';

const TagShopList = props => {
    const [state, setState] = useState();

    useEffect(() => {
        if(props.shopTagList.length !== 0) {
            setState(props.shopTagList);
        }
    }, []);

    const openInfoModal = info => {
        const { name, date, list } = info;
        Modal.info({
            title: name + " | " + date,
            content: (
                <div>
                    {
                        list.map(l => {
                            return <p>{l.item} כמות: {l.amount}</p>;
                        })
                    }
                    
                </div>
            ),
            onOk() {},
        });
    }

    if(state) {
        return (
            <div className="tagShopList">
                <div className="card-container">
                    {
                        state.map(list => {
                            return <button className="card" onClick={() => openInfoModal(list)}>
                                <div className="date">Date: <u>{list.date}</u></div>
                                <div className="tagger">Tagged by: <u>{list.name}</u></div>
                            </button>
                        })
                    }
                </div>
            </div>
        )
    } else {
        return <Spinner />
    }
}

const propsToState = state => (state.daily);
export default connect(propsToState, {})(TagShopList);