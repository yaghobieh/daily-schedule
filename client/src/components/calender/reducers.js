import { ACTIVE_DATE, REMOVE_DATE } from './actions';
import axios from 'axios';

export const activeDate = (foodList, wishList, date) => dispatch => {
    let exactFoodDate = {};
    let exactWishListDate = {};

    // Food list checker
    if(foodList !== undefined) {
        if(foodList.length > 0) {
            exactFoodDate = foodList.filter(f => {
                if(f.date === date) 
                    return f;
            });
        } else if(foodList.date === date) {
            exactFoodDate = foodList;
        } else {
            exactFoodDate = {};
        }
    }

    //Wish list checker
    if(wishList !== undefined) {
        if(wishList.length > 0) {
            exactWishListDate = wishList.filter(w => {
                if(w.date === date) 
                    return w;
            });
        }
    }

    const active = {
        date: date,
        list: exactFoodDate,
        wish: exactWishListDate
    };
    dispatch({
        type: ACTIVE_DATE,
        payload: active
    });
}

export const removeDate = () => dispatch => {
    dispatch({
        type: REMOVE_DATE,
        payload: null
    });
}